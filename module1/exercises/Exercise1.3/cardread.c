#include <altera_up_sd_card_avalon_interface.h>
#include <stdio.h>

int main( void ){
	alt_up_sd_card_dev *device_reference = NULL;
	char *file_name;
	int connected = 0;
	device_reference = alt_up_sd_card_open_dev( "/dev/Altera_UP_SD_Card_Avalon_Interface_0" );

	if( device_reference != NULL ){
		while( 1 ){
			if( ( connected == 0 ) && ( alt_up_sd_card_is_Present() ) ){
				printf( "Card connected.\n" );

				if( alt_up_sd_card_is_FAT16() ){
					printf( "FAT16 file system detected.\n" );

					if( alt_up_sd_card_find_first( "", file_name ) == 0 ){
						printf( "%s\n", file_name );

						while( alt_up_sd_card_find_next( file_name ) == 0 )
							printf( "%s\n", file_name );
					}
				} else{
					printf( "Unknown file system.\n" );
				}
				connected = 1;
			} else if ( ( connected == 1 ) && ( alt_up_sd_card_is_Present() == false ) ){
				printf( "Card disconnected.\n" );
				connected = 0;
			}
		}
	}
	return 0;
}
