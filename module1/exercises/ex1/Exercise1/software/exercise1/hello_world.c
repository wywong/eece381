
//key values: key 3 = 7, key 2 = 11, key 1 = 13, key 0 = 14, no keys = 15

#include <stdio.h>
#include "altera_up_avalon_character_lcd.h"

#define keys (volatile char*) 0x0002020

int main(void)
{

	alt_up_character_lcd_dev * char_lcd_dev;

	// open the Character LCD port
	char_lcd_dev = alt_up_character_lcd_open_dev ("/dev/character_lcd_0");

	if ( char_lcd_dev == NULL)
		alt_printf ("Error: could not open character LCD device\n");
	else
		alt_printf ("Opened character LCD device\n");

	/* Initialize the character display */
	alt_up_character_lcd_init (char_lcd_dev);

	int xPos = 0;
	int yPos = 0;
	char* keyNum = (char *)-1;
	int set = 0;
	int reset = 0;

	while (1){

		if(*keys == 7){
			keyNum = "3";
			set = 1;
		}
		else if(*keys == 11) {
			keyNum = "2";
			set = 1;
		}
		else if(*keys == 13){
			keyNum = "1";
			set = 1;
		}
		else if(*keys == 14) {
			keyNum = "0";
			set = 1;
		}
		else if(*keys == 15 && set == 1) {
			reset = 1;
		}

		if(set == 1 && reset == 1){
			if( xPos == 16 && yPos == 0 ){
				xPos = 0;
				yPos = 1;
			}
			if (xPos == 16 && yPos == 1){
				xPos = 0;
				yPos = 0;
				alt_up_character_lcd_init (char_lcd_dev);
			}

			alt_up_character_lcd_set_cursor_pos(char_lcd_dev, xPos, yPos);
			alt_up_character_lcd_string(char_lcd_dev, keyNum);
			xPos ++;
			set = 0;
			reset = 0;

		}
	}
}
