/* Filename: timer_test.c */
#include <stdio.h>
#include <sys/alt_timestamp.h>

#define M_SIZE 100

volatile static int a[M_SIZE][M_SIZE];
volatile static int b[M_SIZE][M_SIZE];
volatile static int c[M_SIZE][M_SIZE];

void matrix_multiply() {
	int i, j, k, sum;
	for(i = 0; i < M_SIZE; ++i) {
		for(j = 0; j < 100; ++j) {
			sum = 0;
			for(k = 0; k < 100; ++k) {
				sum = sum + a[i][k]*b[k][j];
			}
			c[i][j] = sum;
		}
	}
}

void timer_test(void) {
	int start_time, end_time;
	if(alt_timestamp_start() < 0) {
		printf("ERROR STARTING TIMER\n");
		return;
	}
	start_time = (int) alt_timestamp();
	matrix_multiply();
	end_time = (int) alt_timestamp();
	printf("time taken: %d clock ticks\n", end_time-start_time);
	printf("            %f seconds\n", (float) (end_time-start_time) /
			(float) alt_timestamp_freq());
}

int main() {
	timer_test();
	return 0;
}


