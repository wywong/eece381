#ifndef __QUEUE_H__
#define __QUEUE_H__

#ifndef DEBUG
#define CIRC_BUF_SIZE 1092
#else
#define CIRC_BUF_SIZE 5
#endif


#define X_COORD 0
#define Y_COORD 1

#include <altera_up_sd_card_avalon_interface.h>

typedef struct point {
	int x, y;
} point;

typedef struct queue {
	/* Indices */
	int start;
	int end;

	/* Current size */
	int size;

	/* x coord is [i][0]
	 * y coord is [i][1] */
	int buffer[CIRC_BUF_SIZE][2];
} queue;

/* Creates and returns an empty queue */
queue* createQueue();

/* Checks if queue is empty
 * Returns true if empty.
 * Otherwise returns false */
bool isEmptyQueue(queue* q);

/* enqueue point (x,y) to the end of the queue
 * Return true on success, false if queue is full */
bool enqueue(queue* q, int x, int y);

/* dequeue head of the queue
 * Return true on success, false if queue was empty */
bool dequeue(queue* q);

/* peek at the head of the queue
 * store the result in point p
 * Return false if queue is empty, true otherwise */
bool peek_head(queue* q, point* p);

/* peek at the tail of the queue
 * store the result in point p
 * Return false if queue is empty, true otherwise */
bool peek_tail(queue* q, point* p);


#endif /* __QUEUE_H__ */

