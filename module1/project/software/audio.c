
//TO ADD: 	automatic size read
//			keys for different .wav files


#include "audio.h"
#include "sd_card.h"
#include <stdio.h>
#include <stdlib.h>

alt_up_audio_dev * audio =  NULL;
unsigned int sound_data_counter = 0;
int foodFlag = 0;

unsigned int* data;
unsigned int* backgroundData;
unsigned int* greetData;
unsigned int* foodData;
unsigned int* deathData;
unsigned int* size;
unsigned int* soundBuffer;

char byte_data[2];
unsigned int food_byte_data[2];
short tempData;
short tempFood;

//set up of audio device
void audio_setup(){

	//configure audio and video controller
	alt_up_av_config_dev *av_config = alt_up_av_config_open_dev("/dev/audio_and_video_config_0");
	while (!alt_up_av_config_read_ready(av_config)) {}

	//open the audio port
	audio = alt_up_audio_open_dev(AUDIO_0_NAME);
	if (audio == NULL)
		alt_printf("Error: could not open audio device\n");
	else
		alt_printf("Opened audio device\n");

	alt_up_audio_reset_audio_core(audio);

}

// Reset interrupts and reset audio device
void disable_audio()
{
	sound_data_counter = 0;
	alt_up_audio_disable_write_interrupt(audio);
	alt_up_audio_reset_audio_core(audio);

	free(backgroundData);
	backgroundData = NULL;
	free(greetData);
	greetData = NULL;
	free(foodData);
	foodData = NULL;
	free(deathData);
	deathData = NULL;
	free(soundBuffer);
	soundBuffer = NULL;
}

void assign_data_arrays(void){
	backgroundData = (unsigned int*) malloc(BACKGROUND_SIZE * sizeof(unsigned int));
	greetData = (unsigned int*) malloc(GREET_SIZE * sizeof(unsigned int));
	foodData = (unsigned int*) malloc(FOOD_SIZE * sizeof(unsigned int));
	deathData = (unsigned int*) malloc(DEATH_SIZE * sizeof(unsigned int));
}

//reads the .wav file from SD card and stores it in an array
int load_wav_file(void){
	printf("Loading Sound Files...\n");

	int loop_counter;
	short int fileHandle;

	//assign the data array the memory needed for the wav file size
	assign_data_arrays();

	//OPEN GREET FILE;
	fileHandle = open_file(GREET_FILE_NAME);
	printf(" (1/4)\n");

	//skip header in wav file - 44 bytes long but only skipping 40 bytes to get size of file (size is 4 bytes long)
	for (loop_counter = 0; loop_counter < WAV_HEADER; loop_counter++)
	{
		alt_up_sd_card_read(fileHandle);
	}

	//read and store sound bytes into data
	for (loop_counter = 0; loop_counter < GREET_SIZE; loop_counter++)
	{
		greetData[loop_counter] = alt_up_sd_card_read(fileHandle);
	}

	close_file(fileHandle);

	//OPEN BACKGROUND SOUND FILE
	fileHandle = open_file(BACKGROUND_FILE_NAME);
	printf(" (2/4)\n");
	int background_max = 0;

	//skip header in wav file - 44 bytes long but only skipping 40 bytes to get size of file (size is 4 bytes long)
	for (loop_counter = 0; loop_counter < WAV_HEADER; loop_counter++)
	{
		alt_up_sd_card_read(fileHandle);
	}

	//read and store sound bytes into data
	for (loop_counter = 0; loop_counter < BACKGROUND_SIZE; loop_counter++)
	{
		backgroundData[loop_counter] = alt_up_sd_card_read(fileHandle);
	}
	close_file(fileHandle);

	//OPEN FOOD SOUND FILE
	fileHandle = open_file(FOOD_FILE_NAME);
	printf(" (3/4)\n");
	int food_max = 0;

	//skip header in wav file - 44 bytes long but only skipping 40 bytes to get size of file (size is 4 bytes long)
	for (loop_counter = 0; loop_counter < WAV_HEADER; loop_counter++)
	{
		alt_up_sd_card_read(fileHandle);
	}

	//read and store sound bytes into data
	for (loop_counter = 0; loop_counter < FOOD_SIZE; loop_counter++)
	{
		foodData[loop_counter] = alt_up_sd_card_read(fileHandle);
	}

	close_file(fileHandle);

	//OPEN SNAKE DEATH SOUND FILE
	fileHandle = open_file(DEATH_FILE_NAME);
	printf(" (4/4)\n");

	//skip header in wav file - 44 bytes long but only skipping 40 bytes to get size of file (size is 4 bytes long)
	for (loop_counter = 0; loop_counter < WAV_HEADER; loop_counter++)
	{
		alt_up_sd_card_read(fileHandle);
	}

	//read and store sound bytes into data
	for (loop_counter = 0; loop_counter < DEATH_SIZE; loop_counter++)
	{
		deathData[loop_counter] = alt_up_sd_card_read(fileHandle);
	}

	close_file(fileHandle);

	printf("Finished loading audio files\n");

	//create buffer for storing samples of sound data
	soundBuffer = (unsigned int*) malloc(SAMPLE_SIZE * sizeof(unsigned int));
	return 1;
}




//disable read and write interrupts for Audio Core
void initialize_audio_irq()
{
	printf("Initializing Audio IRQ...\n");

	alt_up_audio_disable_read_interrupt(audio);
	alt_up_audio_disable_write_interrupt(audio);

	printf("Audio IRQ initialized...\n");
}


//interrupt driven playback of .wav file
void play_greet(){
	// register isr
	sound_data_counter = 0;
	alt_irq_register(AUDIO_0_IRQ, 0x0, greet_isr);
	// enable interrupt
	alt_irq_enable(AUDIO_0_IRQ);
	alt_up_audio_enable_write_interrupt(audio);

}

//interrupt driven playback of .wav file
void play_background(){
	// register isr
	sound_data_counter = 0;
	alt_irq_register(AUDIO_0_IRQ, 0x0, background_isr);
	// enable interrupt
	alt_irq_enable(AUDIO_0_IRQ);
	alt_up_audio_enable_write_interrupt(audio);
}

//interrupt driven playback of .wav file
void play_food(){
	foodFlag = 1;
	food_data_counter = 0;

}

//interrupt driven playback of .wav file
void play_death(){
	// register isr
	sound_data_counter = 0;
	alt_irq_register(AUDIO_0_IRQ, 0x0, death_isr);
	// enable interrupt
	alt_irq_enable(AUDIO_0_IRQ);
	alt_up_audio_enable_write_interrupt(audio);
}

//very similar to play_wav however, this function is used with an interrupt
void greet_isr(void* context, alt_u32 id)
{
	int sample_counter;
	for (sample_counter = 0; sample_counter < SAMPLE_SIZE; sample_counter++)
	{
		// take 2 bytes at a time
		byte_data[0] = greetData[sound_data_counter];
		byte_data[1] = greetData[sound_data_counter+1];
		sound_data_counter += 2;

		// combine the two bytes and store into sample buffer
		soundBuffer[sample_counter] = ((unsigned char)byte_data[1] << 8) | (unsigned char)byte_data[0];

		// if we finish reading our data buffer, then we loop back to start over
		if (sound_data_counter >= GREET_SIZE)
		{
			sound_data_counter = 0;
			alt_up_audio_disable_write_interrupt(audio);
		}
	}
	// finally, we write this sample data to the FIFO
	alt_up_audio_write_fifo(audio, soundBuffer, SAMPLE_SIZE, ALT_UP_AUDIO_LEFT);
	alt_up_audio_write_fifo(audio, soundBuffer, SAMPLE_SIZE, ALT_UP_AUDIO_RIGHT);
}

//very similar to play_wav however, this function is used with an interrupt
void food_isr(void* context, alt_u32 id)
{
	int sample_counter;
	for (sample_counter = 0; sample_counter < SAMPLE_SIZE; sample_counter++)
	{
		// take 2 bytes at a time
		byte_data[0] = foodData[sound_data_counter];
		byte_data[1] = foodData[sound_data_counter+1];
		sound_data_counter += 2;

		// combine the two bytes and store into sample buffer
		soundBuffer[sample_counter] = ((unsigned char)byte_data[1] << 8) | (unsigned char)byte_data[0];

		// if we finish reading our data buffer, then we loop back to start over
		if (sound_data_counter >= FOOD_SIZE)
		{
			sound_data_counter = 0;
			alt_up_audio_disable_write_interrupt(audio);
		}
	}
	// finally, we write this sample data to the FIFO
	alt_up_audio_write_fifo(audio, soundBuffer, SAMPLE_SIZE, ALT_UP_AUDIO_LEFT);
	alt_up_audio_write_fifo(audio, soundBuffer, SAMPLE_SIZE, ALT_UP_AUDIO_RIGHT);
}

//very similar to play_wav however, this function is used with an interrupt
void background_isr(void* context, alt_u32 id)
{
	int sample_counter;
	for (sample_counter = 0; sample_counter < SAMPLE_SIZE; sample_counter++)
	{
		 //if play food flag is set, superimpose the two waves
		//once we reach end of FOOD_SIZE (using food_data_counter), turn flag off

		if(foodFlag == 1){
			// take 2 bytes at a time
			byte_data[0] = backgroundData[sound_data_counter];
			byte_data[1] = backgroundData[sound_data_counter+1];
			sound_data_counter += 2;

			food_byte_data[0] = foodData[food_data_counter];
			food_byte_data[1] = foodData[food_data_counter+1];
			food_data_counter +=2;

			// combine the two bytes and store into sample buffer
			tempData = ((unsigned char)byte_data[1] << 8) | (unsigned char)byte_data[0];
			tempFood = ((unsigned char)food_byte_data[1] << 8) | (unsigned char)food_byte_data[0];

			tempData = tempData>>3;
			tempFood = tempFood>>3;
			soundBuffer[sample_counter] = tempData + tempFood;
			//soundBuffer[sample_counter] = ((unsigned char)byte_data[1] << 8) | (unsigned char)byte_data[0];

			if(food_data_counter >= FOOD_SIZE){
				foodFlag = 0;
				food_data_counter = 0;
			}
			if (sound_data_counter >= BACKGROUND_SIZE){
				sound_data_counter = 0;
			}
		}
		else{
			// take 2 bytes at a time
			byte_data[0] = backgroundData[sound_data_counter];
			byte_data[1] = backgroundData[sound_data_counter+1];
			sound_data_counter += 2;

			tempData = ((unsigned char)byte_data[1] << 8) | (unsigned char)byte_data[0];
			tempData = tempData>>3;
			soundBuffer[sample_counter] = tempData;

			// combine the two bytes and store into sample buffer
			//soundBuffer[sample_counter] = ((unsigned char)byte_data[1] << 8) | (unsigned char)byte_data[0];

			// if we finish reading our data buffer, then we loop back to start over by omitting the disable write
			// interrupt function
			if (sound_data_counter >= BACKGROUND_SIZE){
				sound_data_counter = 0;
			}
		}
	}
	// finally, we write this sample data to the FIFO
	alt_up_audio_write_fifo(audio, soundBuffer, SAMPLE_SIZE, ALT_UP_AUDIO_LEFT);
	alt_up_audio_write_fifo(audio, soundBuffer, SAMPLE_SIZE, ALT_UP_AUDIO_RIGHT);
}

//very similar to play_wav however, this function is used with an interrupt
void death_isr(void* context, alt_u32 id)
{
	int sample_counter;
	for (sample_counter = 0; sample_counter < SAMPLE_SIZE; sample_counter++)
	{
		// take 2 bytes at a time
		byte_data[0] = deathData[sound_data_counter];
		byte_data[1] = deathData[sound_data_counter+1];
		sound_data_counter += 2;

		// combine the two bytes and store into sample buffer
		soundBuffer[sample_counter] = ((unsigned char)byte_data[1] << 8) | (unsigned char)byte_data[0];

		// if we finish reading our data buffer, then we loop back to start over
		if (sound_data_counter >= DEATH_SIZE)
		{
			sound_data_counter = 0;
			alt_up_audio_disable_write_interrupt(audio);
		}
	}
	// finally, we write this sample data to the FIFO
	alt_up_audio_write_fifo(audio, soundBuffer, SAMPLE_SIZE, ALT_UP_AUDIO_LEFT);
	alt_up_audio_write_fifo(audio, soundBuffer, SAMPLE_SIZE, ALT_UP_AUDIO_RIGHT);
}
