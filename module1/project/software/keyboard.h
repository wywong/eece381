/*
 * keyboard.h
 *
 *  Created on: Feb 6, 2014
 *      Author: Erin Bush
 */

#ifndef KEYBOARD_H_
#define KEYBOARD_H_

#include "altera_up_avalon_ps2.h"
#include "altera_up_avalon_ps2_regs.h"
#include "altera_up_ps2_keyboard.h"

//ON KEYBOARD I WAS USING:
#define UP_ARROW 0x75
#define DOWN_ARROW 0x72
#define LEFT_ARROW 0x6b
#define RIGHT_ARROW 0x74
#define ENTER 0x5a
#define W_KEY 0x1d
#define A_KEY 0x1c
#define S_KEY 0x1b
#define D_KEY 0x23
#define P_KEY 0x23
#define NUM1 0x16
#define NUM2 0x1e
#define NUM3 0x26
#define NUM4 0x25
#define NUM5 0x2e
#define ESC 0x76

void initializeKeyboard();
void keyboard_interrupt();

#define SCAN_CODE_NUM 102

char *key_table1[SCAN_CODE_NUM] = { "A", "B", "C", "D", "E", "F", "G", "H", "I",
		"J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W",
		"X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "`",
		"-", "=", "\\", "BKSP", "SPACE", "TAB", "CAPS", "L SHFT", "L CTRL",
		"L GUI", "L ALT", "R SHFT", "R CTRL", "R GUI", "R ALT", "APPS",
		"ENTER", "ESC", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9",
		"F10", "F11", "F12", "SCROLL", "[", "INSERT", "HOME", "PG UP",
		"DELETE", "END", "PG DN", "U ARROW", "L ARROW", "D ARROW", "R ARROW",
		"NUM", "KP /", "KP *", "KP -", "KP +", "KP ENTER", "KP .", "KP 0",
		"KP 1", "KP 2", "KP 3", "KP 4", "KP 5", "KP 6", "KP 7", "KP 8", "KP 9",
		"]", ";", "'", ",", ".", "/" };

char ascii_codes1[SCAN_CODE_NUM] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
		'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V',
		'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
		'`', '-', '=', 0, 0x08, 0, 0x09, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x0A,
		0x1B, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '[', 0, 0, 0, 0x7F, 0, 0,
		0, 0, 0, 0, 0, '/', '*', '-', '+', 0x0A, '.', '0', '1', '2', '3', '4',
		'5', '6', '7', '8', '9', ']', ';', '\'', ',', '.', '/' };


#endif /* KEYBOARD_H_ */
