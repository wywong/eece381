/* Filename: snake-driver.c */
#ifdef DEBUG
#include "tests/unit_tests.h"
#endif

#include "altera_avalon_pio_regs.h"
#include "audio.h"
#include "io.h"
#include "menu.h"
#include "profile.h"
#include "sd_card.h"
#include "snake.h"
#include "system.h"
#include "keyboard.h"
#include "tron_graphics.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/alt_irq.h>
#include <unistd.h>

#define KEY0 (0x1)
#define KEY1 (0x2)
#define KEY2 (0x4)
#define KEY3 (0x8)

#define SW0 (0x1)
#define SW1 (0x2)
#define SW2 (0x4)
#define SW3 (0x8)

#define SWITCHES (volatile char*) 0x0004430
#define BUTTONS (volatile char*) 0x0004440

void handle_button_interrupts(void* context, alt_u32 id);
void interrupt_handler(void * context);
void pushbutton_ISR(void);
void getinput(void);
void keyboard_ISR(void *context, alt_u32 id);

int loopNum = 0;
int bufferNum = 0;
int inputID = 0;
char idBuffer[2];
char nameBuffer[10];
int setScore;
int newHighScoreFlag;
int newID = 0;


volatile int edge_capture;

char buffer1[30];
char buffer2[30];
char buffer3[30];
char buffer4[30];
char buffer5[30];

alt_up_ps2_dev* keyboard;

/* Recast the edge_capture pointer to match the alt_irq_register() function
 * * prototype. */
void* edge_capture_ptr = (void*) &edge_capture;

void init_button_interrupt() {
	/* Enable first four interrupts. */
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(BUTTONS, 0xf);

	/* Reset the edge capture register. */
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(BUTTONS, 0xFF);

	/* Register the interrupt handler. */
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
	alt_ic_isr_register(0, KEYS_IRQ, handle_button_interrupts,
			edge_capture_ptr, NULL);
	alt_ic_irq_enable(0, KEYS_IRQ);
#else
	alt_irq_register(KEYS_IRQ, edge_capture_ptr, handle_button_interrupts);
	alt_irq_enable(KEYS_IRQ);
#endif
}

volatile int nextDirS1;
volatile int nextDirS2;
volatile int *curDirS1;
volatile int *curDirS2;
volatile int menu = MAIN_MENU;
volatile int menuChoice = CHOOSE_SINGLE_PLAYER;
volatile int nextMenuChoice;

void handle_button_interrupts(void* context, alt_u32 id) {
	/* Cast context to edge_capture's type. It is important that this be
	 * * declared volatile to avoid unwanted compiler optimization.
	 * */

	volatile int* edge_capture_ptr = (volatile int*) context;
	/* Store the value in the Button's edge capture register in *context. */
	*edge_capture_ptr =
		IORD_ALTERA_AVALON_PIO_EDGE_CAP(BUTTONS);
	if(*edge_capture_ptr & KEY0) {
		/* Select menu option */
		if(menu == MAIN_MENU && menuChoice == CHOOSE_SINGLE_PLAYER) {
			menu = SINGLE_PLAYER_SCREEN;
		} else if(menu == MAIN_MENU && menuChoice == CHOOSE_TWO_PLAYER) {
			menu = TWO_PLAYER_SCREEN;
		} else if(menu == MAIN_MENU && menuChoice == CHOOSE_PROFILES) {
			menu = PROFILE_SCREEN;
		} else if(menu == MAIN_MENU && menuChoice == CHOOSE_TUTORIAL) {
			menu = TUTORIAL_SCREEN;
		} else if(menu == MAIN_MENU && menuChoice == CHOOSE_GLUTTONY) {
			menu = GLUTTONY_SCREEN;
		} else if(menu == GAME_OVER_SCREEN) {
			menu = MAIN_MENU;
		} else {
			nextDirS1 = (*curDirS1 + 1 ) % 4;
		}
	} else if (*edge_capture_ptr & KEY1) {
		/* If key0 is hit make choice based on menu selection */
		if(menu == MAIN_MENU) {
			nextMenuChoice = ((menuChoice + 1) % NUM_MENU_OPTIONS);
		} else if(menu == PROFILE_SCREEN) {
			menu = MAIN_MENU;
		} else {
			nextDirS1 = (*curDirS1 + 3 ) % 4;
		}
	} else if (*edge_capture_ptr & KEY2) {
			nextDirS2 = (*curDirS2 + 1 ) % 4;
	} else if (*edge_capture_ptr & KEY3) {
			nextDirS2 = (*curDirS2 + 3 ) % 4;
	}
	/*	Reset the Button's edge capture register. */
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(BUTTONS, 0xf);
}

/*
 * Initialization of keyboard controller
 */
void initializeKeyboard() {
	printf("Initialize keyboard...\n");
	keyboard = alt_up_ps2_open_dev(KEYBOARD_0_NAME);
	if (keyboard == NULL)
	{
		printf("Error: could not open device\n");
	}
	else
	{
		printf("Opened Keyboard\n");
	}

	alt_up_ps2_init(keyboard);
	alt_up_ps2_clear_fifo(keyboard);
	alt_up_ps2_write_data_byte(keyboard, 0xff);

	printf("Done keyboard initialization...\n");
}



void keyboard_interrupt() {
	void* keyboard_register_ptr = (void*) (KEYBOARD_0_BASE + 4);
	alt_irq_register(KEYBOARD_0_IRQ, keyboard_register_ptr, keyboard_ISR);
	alt_up_ps2_enable_read_interrupt(keyboard);
}

/* keyboard interrupt handler */
void keyboard_ISR(void *context, alt_u32 id) {
	alt_u8 byte;
	KB_CODE_TYPE decode_mode;
	char ascii;
	unsigned idx;

	/* KEYBOARD COMMANDS:

	 * SCROLLING IN MAIN MENU: UP OR DOWN ARROWS
	 * SINGLE PLAYER LEFT AND RIGHT: LEFT ARROW AND RIGHT ARROW
	 * TWO PLAYER LEFT AND RIGHT: A AND D KEY
	 * ENTER INTO A PLAY MODE: ENTER KEY
	 * LEAVE PROFILE MENU: ESC KEY

	 * IF YOU WANT TO CHANGE THE KEYS THAT ARE PRESSED,
	 * THE BYTE VALUES ARE CURRENTLY BEING DISPLAYED ON THE NIOS II CONSOLE
	 * (BYTE VALUE IS WHAT YOU WOULD CHANGE FOR A DIFFERENT KEY) */

	decode_scancode(keyboard, &decode_mode, &byte, &ascii);
	if(byte != 0xaa) {
		if(loopNum == 0) {
			loopNum++;
		}else{
			idx = get_single_byte_make_code_index(byte);
			printf("key pressed: %s byte: %x \n", key_table1[idx], byte);
			loopNum = 0;



			switch((int) byte) {
				case ENTER:
					// Select menu option
					if(menu == MAIN_MENU && menuChoice == CHOOSE_SINGLE_PLAYER) {
						menu = SINGLE_PLAYER_SCREEN;
					} else if(menu == MAIN_MENU && menuChoice == CHOOSE_TWO_PLAYER) {
						menu = TWO_PLAYER_SCREEN;
					} else if(menu == MAIN_MENU && menuChoice == CHOOSE_PROFILES) {
						menu = PROFILE_SCREEN;
					} else if(menu == MAIN_MENU && menuChoice == CHOOSE_TUTORIAL) {
						menu = TUTORIAL_SCREEN;
					} else if(menu == MAIN_MENU && menuChoice == CHOOSE_GLUTTONY) {
						menu = GLUTTONY_SCREEN;
					} else if(menu == GAME_OVER_SCREEN) {
						menu = MAIN_MENU;
					}
					break;
				case RIGHT_ARROW:
					if (menu == SINGLE_PLAYER_SCREEN || menu == TWO_PLAYER_SCREEN
							|| menu == GLUTTONY_SCREEN || menu == TUTORIAL_SCREEN) {
						nextDirS1 = (*curDirS1 + 1 ) % 4;
					}
					break;
				case DOWN_ARROW:
					// If key0 is hit make choice based on menu selection */
					if(menu == MAIN_MENU) {
						nextMenuChoice = ((menuChoice + 1) % NUM_MENU_OPTIONS);
					}
					break;
				case UP_ARROW:
					if(menu == MAIN_MENU) {
						nextMenuChoice = (menuChoice + NUM_MENU_OPTIONS - 1) % NUM_MENU_OPTIONS;
					}
					break;
				case ESC:
					if(menu == PROFILE_SCREEN) {
						menu = MAIN_MENU;
					}
					break;
				case LEFT_ARROW:
					if (menu == SINGLE_PLAYER_SCREEN || menu == TWO_PLAYER_SCREEN
							|| menu == GLUTTONY_SCREEN || menu == TUTORIAL_SCREEN) {
						nextDirS1 = (*curDirS1 + 3 ) % 4;
					}
					break;
				case D_KEY:
					if (menu == TWO_PLAYER_SCREEN || menu == GLUTTONY_SCREEN) {
						nextDirS2 = (*curDirS2 + 1 ) % 4;
					}
					break;
				case A_KEY:
					if (menu == TWO_PLAYER_SCREEN || menu == GLUTTONY_SCREEN) {
						nextDirS2 = (*curDirS2 + 3 ) % 4;
					}
					break;
				case NUM1:
					if (menu == SINGLE_PLAYER_SCREEN && newID == 0) {
						newID = 1;
					}
					break;
				case NUM2:
					if (menu == SINGLE_PLAYER_SCREEN && newID == 0) {
						newID = 2;
					}
					break;
				case NUM3:
					if (menu == SINGLE_PLAYER_SCREEN && newID == 0) {
						newID = 3;
					}
					break;
				case NUM4:
					if (menu == SINGLE_PLAYER_SCREEN && newID == 0) {
						newID = 4;
					}
				break;
				case NUM5:
					if (menu == SINGLE_PLAYER_SCREEN && newID == 0) {
						newID = 5;
					}
				break;
				default:
					break;
			}

		}
	}
}


/* Grid initialized to 0
 */
int GRID[NROW][NCOL] = { {0} };
char* playerDied;


int main(void) {
/*
	int id = 1;
	int score = 700;
	SD_setup();
	checkHighscore(score, id );
	read_profiles();

	*/
#ifndef DEBUG
	int audioInitialized = 0;
	while(true)
	{
		// Initialize menu
		int loadAudio = 0;
		int loadBMP = 0;
		int loadBackground = 0;

		if(*SWITCHES & SW3) {
			initializeKeyboard();
			keyboard_interrupt();
		} else {
			init_button_interrupt();
		}

		initBuffers();
		writeString(INITIALIZING, 34, 29);
		if(audioInitialized == 0)
		{
			 // Initialize audio
			audio_setup();
			SD_setup();
			loadBMP = load_bmp_file();



			loadAudio = load_wav_file();
			initialize_audio_irq();
			audioInitialized++;
			while(!loadAudio);
			while(!loadBMP);
			loadBackground = loadMenuBackground();
			while(!loadBackground);
		}
		play_greet();

		printHighscore( buffer1, buffer2, buffer3, buffer4, buffer5);

		clearCharBuff();
		resetGrid();

		//Title Menu Logic
		while(menu < 3)
		{
			clearCharBuff();
			// Title Menu
			while(menu == MAIN_MENU)
			{
				writeString(SINGLE_PLAYER_OPTION, 34, 29);
				writeString(TWO_PLAYER_OPTION, 34, 35);
				writeString(PROFILE_OPTION, 34, 41);
				writeString(TUTORIAL_OPTION, 34, 47);
				writeString(GLUTTONY_OPTION, 34, 53);
				menuChoice = nextMenuChoice;
				menuSelector(menuChoice);
			}
			clearAllBuffs();
			// Player Scores
			while(menu == PROFILE_SCREEN)
			{
				//TO DO: IMPLEMENT DIFFERENTLY SO THAT IT DOESN'T KEEP ALLOCATING MEMORY
				writeString(HIGHSCORES, 38, 8);
				//writeString(EDITPROFILE, 30, 15);
				//printHighscore(buffer);

				printHighscoreScreen( buffer1, buffer2, buffer3, buffer4, buffer5);
			}

		}
		if (menu == SINGLE_PLAYER_SCREEN) {
			clearAllBuffs();
			writeString("Please enter your player ID", 32, 24);
			newID = 0;
			while(!newID);

			printf("newID: %i\n", newID);
			writeHighscore(newID,setScore);

			read_profiles();
			printHighscore( buffer1, buffer2, buffer3, buffer4, buffer5);
		}
		// Start Game
		play_background();
		initBuffers();

		if (menu == SINGLE_PLAYER_SCREEN) {
			singlePlayer();
			menu = GAME_OVER_SCREEN;
		}

		if (menu == TWO_PLAYER_SCREEN) {
			twoPlayer();
			menu = GAME_OVER_SCREEN;
		}

		if (menu == TUTORIAL_SCREEN) {
			tutorial();
			menu = GAME_OVER_SCREEN;
		}

		if (menu == GLUTTONY_SCREEN) {
			gluttony();
			menu = GAME_OVER_SCREEN;
		}

		// Free audio data on exit
		if(audioInitialized == 0)
		{
			disable_audio();
			printf("Audio Disabled\n");
		}

		// Print Game Over Screen
		clearAllBuffs();
		writeString(GAME_OVER, 36, 8);
		writeString(playerDied, 32, 24);
		writeString(RESTART, 32, 31);
		while(menu == GAME_OVER_SCREEN);

	}
#else
	create_test();
	empty_test();

	enqueue_test();
	dequeue_test();

	peek_head_test();
	peek_tail_test();

	printGrid();
	resetGrid();
	printGrid();

	snake* s = createSnake(1, FPS, EAST);
	while(true) {
		printGrid();
		if(!snakeUpdate(s)) break;
	}
	printGrid();

	readprofile_test();
#endif

	return 0;
}
