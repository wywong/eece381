
#include "profile.h"
#include "sd_card.h"
#include "tron_graphics.h"
#include <altera_up_sd_card_avalon_interface.h>
#include <stdio.h>
#include <stdlib.h>

unsigned int* profile_data;
player* player_array[5];

extern int inputID;
extern char idBuffer[];
extern char nameBuffer[];

/* printProfile
 * Return: Returns true on successful write to the SD card
 * Writes the player data to "profile.txt" on the SD card
 */
bool printProfile(player *p) {
	char buffer[BUFFERSIZE];
	int i;
	short int fileHandle;

	if(sprintf(buffer, "%i%i", p->playerID, p->highscore) !=
			BUFFERSIZE) {
		return false;
	}

	for(i = 0; i < BUFFERSIZE; i++) {
		printf("%c", buffer[i]);
	}
	printf("\n");

	fileHandle = open_file("profile.txt");
	if(fileHandle < 0) {
		return false;
	} else {
		for(i = 0; i < BUFFERSIZE; i++) {
			alt_up_sd_card_write(fileHandle, buffer[i]);
		}

		alt_up_sd_card_fclose(fileHandle);
	}

	return true;
}

//WRITE PROFILE: adds new line in profiles.txt
//SORT PROFILES: for printing in profiles menu

/* readProfile
 * Return: Returns true on successful read of a profile on the SD card
 * Reads the profile information of player "id" and stores the information in p
 * NOTE: Player ID should NOT be 0
 */
bool readProfile(player *p, int id) {
	/* Initialized to SIZE + 1 for null character */
	char IDBuffer[IDSIZE + 1];
	char HSBuffer[HIGHSCORESIZE + 1];
	int i;
	short int fileHandle;

	fileHandle = open_file("profile1.txt");
	if(fileHandle < 0) {
		return false;
	} else {
		do {
			for(i = 0; i < IDSIZE; i++) {
				IDBuffer[i] = (char)alt_up_sd_card_read(fileHandle);
			}
			IDBuffer[IDSIZE] = '\0';

			p->playerID = atoi(IDBuffer);

			/* Read space */
			alt_up_sd_card_read(fileHandle);

			for(i = 0; i < HIGHSCORESIZE; i++) {
				HSBuffer[i] = (char)alt_up_sd_card_read(fileHandle);
			}
			HSBuffer[HIGHSCORESIZE] = '\0';

			p->highscore = atoi(HSBuffer);

			/* Read carriage return */
			alt_up_sd_card_read(fileHandle);
			alt_up_sd_card_read(fileHandle);
		} while(p->playerID != id && p->playerID != 0);

		if(p->playerID == 0) return false;

		alt_up_sd_card_fclose(fileHandle);
	}

	return true;
}


/* printHighscore
 * Reads the player profiles from the SD card 1 by 1, and prints to screen
 */
void printHighscore(char* buffer1,char* buffer2,char* buffer3,char* buffer4,char* buffer5) {
	int id;
	player p;

	for (id = 1; id<6; id++){
		readProfile(&p, id);

		if(id == 1){
			sprintf(buffer1, "Player %i: %i", p.playerID, p.highscore);
			printf( "%s\n", buffer1 );
		}
		if(id == 2){
			sprintf(buffer2, "Player %i: %i", p.playerID, p.highscore);
			printf( "%s\n", buffer2 );
		}
		if(id == 3){
			sprintf(buffer3, "Player %i: %i", p.playerID, p.highscore);
			printf( "%s\n", buffer3 );
		}
		if(id == 4){
			sprintf(buffer4, "Player %i: %i", p.playerID, p.highscore);
			printf( "%s\n", buffer4 );
		}
		if(id == 5){
			sprintf(buffer5, "Player %i: %i", p.playerID, p.highscore);
			printf( "%s\n", buffer5 );
		}
	}

	return;
}

void printHighscoreScreen(char* buffer1,char* buffer2,char* buffer3,char* buffer4,char* buffer5){
	writeString(buffer1, 34, 20);
	writeString(buffer2, 34, 25);
	writeString(buffer3, 34, 30);
	writeString(buffer4, 34, 35);
	writeString(buffer5, 34, 40);
}

void readHighscore(char* buffer){
	int id;
	player p;
	for (id = 1; id<6; id++){
		readProfile(&p, id);
		sprintf(buffer, "Player %i: %i", p.playerID, p.highscore);
		printf( "%s\n", buffer );
	}
}

bool checkHighscore(int score, int id){
	/* Initialized to SIZE + 1 for null character */
	player *p;
	int playerIdentification  = id;
	char IDBuffer[IDSIZE + 1];
	char HSBuffer[HIGHSCORESIZE + 1];
	int i;
	short int fileHandle;
	printf("\nID: %i\n", id);

	fileHandle = open_file("profile1.txt");
	if(fileHandle < 0) {
		return false;
	} else {
		do {
			for(i = 0; i < IDSIZE; i++) {
				IDBuffer[i] = (char)alt_up_sd_card_read(fileHandle);
				printf("%x", IDBuffer[i]);
			}
			IDBuffer[IDSIZE] = '\0';

			printf("\n");

			p->playerID = atoi(IDBuffer);

			printf("\np->playerID: %x, ID: %i, Pid: %i",p->playerID, id, playerIdentification);

			/* Read space */
			alt_up_sd_card_read(fileHandle);

			for(i = 0; i < HIGHSCORESIZE; i++) {
				HSBuffer[i] = (char)alt_up_sd_card_read(fileHandle);
			}
			HSBuffer[HIGHSCORESIZE] = '\0';

			p->highscore = atoi(HSBuffer);

			/* Read carriage return */
			alt_up_sd_card_read(fileHandle);
			alt_up_sd_card_read(fileHandle);

		} while(p->playerID != id);

		printf("\np->highscore = %i, score = %i\n", p->highscore, score);

		alt_up_sd_card_fclose(fileHandle);

		if ( p->highscore > score){
			p->highscore = score;
			printf("New highscore!!!\n");
			//highscoreScreen();
			writeHighscore( p->playerID, p->highscore );
		}

		if(p->playerID == 0) return false;
	}

	return true;
}



bool writeHighscore( int id, int score){

	/* Initialized to SIZE + 1 for null character */
	char IDBuffer[IDSIZE + 1];
	char HSBuffer[HIGHSCORESIZE + 1];
	int i;
	short int fileHandle;
	char buffer[3];
	int n;
	player* p;

	fileHandle = open_file("profile1.txt");
	if(fileHandle < 0) {
		return false;
	} else {
		do {
			for(i = 0; i < IDSIZE; i++) {
				IDBuffer[i] = (char)alt_up_sd_card_read(fileHandle);
			}
			IDBuffer[IDSIZE] = '\0';

			p->playerID = atoi(IDBuffer);

			/* Read space */
			alt_up_sd_card_read(fileHandle);

			if(p->playerID != id){
				for(i = 0; i < HIGHSCORESIZE; i++) {
					HSBuffer[i] = (char)alt_up_sd_card_read(fileHandle);
				}
				HSBuffer[HIGHSCORESIZE] = '\0';

				p->highscore = atoi(HSBuffer);

				/* Read carriage return */
				alt_up_sd_card_read(fileHandle);
				alt_up_sd_card_read(fileHandle);
			}

		} while(p->playerID != id && p->playerID != 0);

		n = sprintf (buffer, "%03d", score);
		printf("\nhighscore: %s\n", buffer);

		for(i = 0; i < HIGHSCORESIZE; i++)
		{
			alt_up_sd_card_write(fileHandle, buffer[i]);
		}

		if(p->playerID == 0) return false;

		alt_up_sd_card_fclose(fileHandle);
	}

	return true;

	//if player is not in file, add player

	printf("\nDone writing to file...\n");
}


void read_profiles(void){
	int fileHandle, loop_counter;
	fileHandle = open_file(FILE_NAME);
	profile_data = (unsigned int*) malloc(FILE_SIZE * sizeof(unsigned int));
	//*player_array = (unsigned int*) malloc(NUM * sizeof(player*));
	printf("\n");

	//read and store sound bytes into data
	for (loop_counter = 0; loop_counter < FILE_SIZE; loop_counter++)
	{
		profile_data[loop_counter] = alt_up_sd_card_read(fileHandle);
		printf("%c", profile_data[loop_counter]);
	}
	printf("\n\n");

	//number of players to make is FILE_SIZE/8
	//player* player_array[FILE_SIZE/8];
	int i = 0;

	//STORE CURRENT HIGH SCORES IN PLAYER STRUCT FROM .TXT FILE
	for (loop_counter = 0; loop_counter < FILE_SIZE; loop_counter++)
	{
		if(loop_counter%LINE_LENGTH == 0){
			player_array[i]->playerID = profile_data[loop_counter]<<8|profile_data[loop_counter+1];
			printf("player[%i]->playerID = %x   ", i, player_array[i]->playerID);
		}
		if(loop_counter%LINE_LENGTH == 3){
			player_array[i]->highscore = (profile_data[loop_counter]-48)*100 + (profile_data[loop_counter+1]-48)*10 + (profile_data[loop_counter+2]-48);
			printf("player[%i]->highscore = %i\n", i, player_array[i]->highscore);
			i++;
		}
		//printf("%x ", profile_data[loop_counter]);
	}

	close_file(fileHandle);
}

void writeFile(void){
	char string[] = "test";
	char space[] = " ";
	char enter[] = {0xda};

	int bytes_written = 0;

	//open our score file
	int fileHandle;
	fileHandle = open_file(FILE_NAME);
	int i, size;

	size = strlen(string);
	for(i = 0; i < size; i++)
	{
		alt_up_sd_card_write(fileHandle, string[i]);
		bytes_written ++;
	}

	alt_up_sd_card_write(fileHandle, enter[0]);
	alt_up_sd_card_write(fileHandle, space[0]);
	alt_up_sd_card_write(fileHandle, enter[0]);

	for(i = 0; i < size; i++)
	{
		alt_up_sd_card_write(fileHandle, string[i]);
		bytes_written ++;
	}

	 //close the file
	if (!close_file(fileHandle)){
		printf("Could not close file properly...\n");
	}

	printf("\nDone writing to file...\n");

}


bool writeProfile(char* name, char* idstring){

	/* Initialized to SIZE + 1 for null character */
	char IDBuffer[IDSIZE + 1];
	int i;
	short int fileHandle;
	player* p;

	int id = atoi(idstring);

	fileHandle = open_file("profile1.txt");
	if(fileHandle < 0) {
		return false;
	} else {
		do {
			for(i = 0; i < IDSIZE; i++) {
				IDBuffer[i] = (char)alt_up_sd_card_read(fileHandle);
			}
			IDBuffer[IDSIZE] = '\0';

			p->playerID = atoi(IDBuffer);

			/* Read space */
			alt_up_sd_card_read(fileHandle);

		} while(p->playerID != id && p->playerID != 0);

		for(i = 0; i < NAMESIZE; i++)
		{
			alt_up_sd_card_write(fileHandle, name[i]);
		}

		if(p->playerID == 0) return false;

		alt_up_sd_card_fclose(fileHandle);
	}

	return true;

	//if player is not in file, add player

	printf("\nDone writing to file...\n");
}


