/*
 * sd_card.h
 *
 *  Created on: 2014-02-06
 *      Author: Brendan
 */

#ifndef SD_CARD_H_
#define SD_CARD_H_

#include <altera_up_sd_card_avalon_interface.h>

static const char MENU_BACKGROUND[] = "tron.bmp";



void SD_setup(void);
bool close_file(short int fileHandle);
short int open_file(char* FILE_NAME);



#endif /* SD_CARD_H_ */
