
#ifndef AUDIO_H_
#define AUDIO_H_

#include <altera_up_avalon_audio_and_video_config.h>
#include <altera_up_avalon_audio.h>
#include <altera_up_sd_card_avalon_interface.h>
#include <stdio.h>
#include <sys/alt_irq.h>

static const int WAV_HEADER = 44;
static const int GREET_SIZE = 780844;
static const int FOOD_SIZE = 74330;
static const int BACKGROUND_SIZE = 591634;
static const int DEATH_SIZE = 237626;
static const int SAMPLE_SIZE = 34;

int food_data_counter;

static const char BACKGROUND_FILE_NAME[] = "tronback.wav";
static const char GREET_FILE_NAME[] = "light.wav";
static const char FOOD_FILE_NAME[] = "food.wav";
static const char DEATH_FILE_NAME[] = "death.wav";


void audio_setup();
void disable_audio();

void assign_data_arrays(void);
int load_wav_file(void);

void initialize_audio_irq();

void play_greet();
void greet_isr(void* context, alt_u32 id);

void play_background();
void background_isr(void* context, alt_u32 id);

void play_food();
void food_isr(void* context, alt_u32 id);

void play_death();
void death_isr(void* context, alt_u32 id);


#endif /* AUDIO_H_ */
