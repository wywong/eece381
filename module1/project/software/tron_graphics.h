/*
 * tron_graphics.h
 *
 *  Created on: 2014-01-26
 *      Author: Brendan
 */

#ifndef TRON_GRAPHICS_H_
#define TRON_GRAPHICS_H_

#ifndef DEBUG

#define NROW 30
#define NCOL 41
#define WALL_CELL 100
#define FOOD_CELL 101
#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 240

#else

#define NROW 10
#define NCOL 10
#define WALL_CELL 8
#define FOOD_CELL 9

#endif

#define NUM_PLAYERS 2

#define EMPTY_CELL 0

#define FPS 10

#define BLACK 0x0000
#define WHITE 0xFFFF
#define RED 0xF800
#define CYAN 0x07FF
#define CELL_SIZE 8

#define BMP_DATA_OFFSET 12
#define BMP_HEADER 14
#define BMP_HEIGHT_OFFSET 8
#define BMP_WIDTH_OFFSET 4
#define HEIGHT_MASK 0x000000FF
#define MENU_BACKGROUND "TronMenu.bmp"
#define MENU_GRAPHIC_SIZE 230454

#include "menu.h"

/* Function Declaration */
/* Initialze the character and pixel buffers for writing */
void initBuffers(void);
/* Swaps the foreground and background buffers */
void swapBuffers(void);
/* Draws an 8x8 cell starting at the given xy coordinates with the given colour value */
void drawCell(int x, int y, int colour);
/* Draws a white border around play area */
void drawBorder(void);
/* Writes a character string to the given xy coordinates */
void writeString(char* string, int x, int y);
/* Clears the backbuffer to all black */
void clearBackBuff(void);
/* Clear Character buffer */
void clearCharBuff(void);
/* Clear All display buffers */
void clearAllBuffs(void);
/* Display the menu GUI choices */
void menuSelector(int menuChoice);
/* Draws a pixel to the background buffer at the given xy coordinates with the given colour */
int draw_pixel_fast(unsigned int color, unsigned int x, unsigned int y);
/* Draws the etank starting at the given xy coordinate */
void drawETank(int x, int y);
/* Draws the superEtank starting at the given xy coordinate */
void drawSuperETank(int x, int y);
/* Draws the Ftank starting at the given xy coordinate */
void drawFTank(int x, int y);
/* Draw Car going North */
void drawCarN(int x, int y, int car);
/* Draw Car going West */
void drawCarW(int x, int y, int car);
/* Draw Car going East */
void drawCarE(int x, int y, int car);
/* Draw Car going South */
void drawCarS(int x, int y, int car);
/* Load bitmap files into memory */
int load_bmp_file(void);
/* Prints the background image into an array */
int loadMenuBackground(void);
/* Draws the menu graphic */
void drawTronMenu(void);
#endif /* TRON_GRAPHICS_H_ */
