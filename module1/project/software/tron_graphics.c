/*
 * VGA
 * Display functions tron graphics
 */

#include <stdio.h>
#include <stdlib.h>
#include <system.h>
#include "io.h"
#include "sd_card.h"
#include "snake.h"
#include "tron_graphics.h"
#include "altera_up_avalon_video_pixel_buffer_dma.h"
#include "altera_up_avalon_video_character_buffer_with_dma.h"

// Declare buffer pointer
alt_up_char_buffer_dev *char_buffer;
alt_up_pixel_buffer_dma_dev* pixel_buffer;

unsigned char* menuImage;
unsigned int tronMenu[240][320] = {{0}};

int tronCarCyan[8][8] = {
	{0x0000,0x0000,0x471d,0xffff,0xffff,0x471d,0x0000,0x0000},
	{0x0000,0x471d,0x471d,0x0000,0x0000,0x471d,0x471d,0x0000},
	{0x0000,0x471d,0x471d,0x0000,0x0000,0x471d,0x471d,0x0000},
	{0x0000,0x0000,0x471d,0x0000,0x0000,0x471d,0x0000,0x0000},
	{0x0000,0x0000,0x471d,0x0000,0x0000,0x471d,0x0000,0x0000},
	{0x0000,0x471d,0x471d,0x471d,0x471d,0x471d,0x471d,0x0000},
	{0x0000,0x471d,0x471d,0xffff,0xffff,0x471d,0x471d,0x0000},
	{0x0000,0x0000,0x471d,0xffff,0xffff,0x471d,0x0000,0x0000}
};

int tronCarRed[8][8] = {
	{0x0000,0x0000,0xf800,0xffff,0xffff,0xf800,0x0000,0x0000},
	{0x0000,0xf800,0xf800,0x0000,0x0000,0xf800,0xf800,0x0000},
	{0x0000,0xf800,0xf800,0x0000,0x0000,0xf800,0xf800,0x0000},
	{0x0000,0x0000,0xf800,0x0000,0x0000,0xf800,0x0000,0x0000},
	{0x0000,0x0000,0xf800,0x0000,0x0000,0xf800,0x0000,0x0000},
	{0x0000,0xf800,0xf800,0xf800,0xf800,0xf800,0xf800,0x0000},
	{0x0000,0xf800,0xf800,0xffff,0xffff,0xf800,0xf800,0x0000},
	{0x0000,0x0000,0xf800,0xffff,0xffff,0xf800,0x0000,0x0000}
};

int Etank[8][8] = {
	{0x0410,0x0410,0x0410,0x07ff,0x0410,0x07ff,0x0410,0x0410},
	{0x0000,0x0410,0x07ff,0x0410,0x07ff,0x07ff,0x0410,0x0000},
	{0x0000,0x0410,0x0000,0x0000,0x0000,0x0000,0x0410,0x0000},
	{0x0000,0x0410,0x0000,0x07ff,0x07ff,0x07ff,0x0410,0x0000},
	{0x0000,0x0410,0x0000,0x0000,0x0000,0x07ff,0x0410,0x0000},
	{0x0000,0x0410,0x0000,0x07ff,0x07ff,0x07ff,0x0410,0x0000},
	{0x0000,0x0410,0x0000,0x0000,0x0000,0x0000,0x0410,0x0000},
	{0x0410,0x0410,0x07ff,0x0410,0x07ff,0x0410,0x0410,0x0410}
};

int superEtank[8][8] = {
	{0xbda0,0xbda0,0xbda0,0xff80,0xbda0,0xff80,0xbda0,0xbda0},
	{0x0000,0xbda0,0xff80,0xbda0,0xff80,0xff80,0xbda0,0x0000},
	{0x0000,0xbda0,0x0000,0x0000,0x0000,0x0000,0xbda0,0x0000},
	{0x0000,0xbda0,0x0000,0xff80,0xff80,0xff80,0xbda0,0x0000},
	{0x0000,0xbda0,0x0000,0x0000,0x0000,0x0000,0xbda0,0x0000},
	{0x0000,0xbda0,0xff80,0xff80,0xff80,0x0000,0xbda0,0x0000},
	{0x0000,0xbda0,0x0000,0x0000,0x0000,0x0000,0xbda0,0x0000},
	{0xbda0,0xbda0,0xff80,0xff80,0xff80,0xbda0,0xbda0,0xbda0}
};

int Ftank[8][8] = {
	{0xb883,0xb883,0xb883,0xf800,0xb883,0xf800,0xb883,0xb883},
	{0x0000,0xb883,0xf800,0xb883,0xf800,0xf800,0xb883,0x0000},
	{0x0000,0xb883,0x0000,0x0000,0x0000,0x0000,0xb883,0x0000},
	{0x0000,0xb883,0x0000,0xf800,0xf800,0xf800,0xb883,0x0000},
	{0x0000,0xb883,0x0000,0x0000,0x0000,0xf800,0xb883,0x0000},
	{0x0000,0xb883,0x0000,0xf800,0xf800,0xf800,0xb883,0x0000},
	{0x0000,0xb883,0x0000,0xf800,0xf800,0xf800,0xb883,0x0000},
	{0xb883,0xb883,0xf800,0xb883,0xf800,0xb883,0xb883,0xb883}
};

/* Initialze the character and pixel buffers for writing */
void initBuffers(void)
{
	// Use the name of your pixel buffer DMA core
	pixel_buffer =	alt_up_pixel_buffer_dma_open_dev(PIXEL_BUFFER_DMA_NAME);
	unsigned int pixel_buffer_addr1 = PIXEL_BUFFER_BASE;
	unsigned int pixel_buffer_addr2 = PIXEL_BUFFER_BASE + (320 * 240 * 2);
	// Set the 1st buffer address
	alt_up_pixel_buffer_dma_change_back_buffer_address(pixel_buffer, pixel_buffer_addr1);
	// Swap buffers � we have to swap because there is only an API function
	// to set the address of the background buffer.
	alt_up_pixel_buffer_dma_swap_buffers(pixel_buffer);
	while (alt_up_pixel_buffer_dma_check_swap_buffers_status(pixel_buffer));
	// Set the 2nd buffer address
	alt_up_pixel_buffer_dma_change_back_buffer_address(pixel_buffer, pixel_buffer_addr2);
	// Clear both buffers (this makes all pixels black)
	alt_up_pixel_buffer_dma_clear_screen(pixel_buffer, 0);
	alt_up_pixel_buffer_dma_clear_screen(pixel_buffer, 1);

	char_buffer = alt_up_char_buffer_open_dev("/dev/char_drawer");
	alt_up_char_buffer_init(char_buffer);
	alt_up_char_buffer_clear(char_buffer);
}

/* Swaps the foreground and background buffers */
void swapBuffers(void)
{
	alt_up_pixel_buffer_dma_swap_buffers(pixel_buffer);
	while (alt_up_pixel_buffer_dma_check_swap_buffers_status(pixel_buffer));
}

/* Draws an 8x8 cell starting at the given xy coordinates with the given colour value */
void drawCell(int x, int y, int colour)
{
	alt_up_pixel_buffer_dma_draw_box(pixel_buffer, x, y, x+CELL_SIZE, y+CELL_SIZE, colour, 1);
}

/* Draws a white border around play area */
void drawBorder(void)
{
	alt_up_pixel_buffer_dma_draw_rectangle(pixel_buffer, 0, 0, 39*CELL_SIZE, 28*CELL_SIZE, WHITE, 1);
}

/* Writes a character string to the given xy coordinates */
void writeString(char* string, int x, int y)
{
	// Write some text
	if(alt_up_char_buffer_string(char_buffer, string, x, y) != 0)
		printf("Character Buffer Print Fail/n");
}

/* Clears the backbuffer to all black */
void clearBackBuff(void)
{
	alt_up_pixel_buffer_dma_clear_screen(pixel_buffer, 1);
}

/* Clear Character buffer */
void clearCharBuff(void)
{
	alt_up_char_buffer_clear(char_buffer);
}

/* Clear Character buffer */
void clearAllBuffs(void)
{
	alt_up_char_buffer_clear(char_buffer);
	alt_up_pixel_buffer_dma_clear_screen(pixel_buffer, 0);
	alt_up_pixel_buffer_dma_clear_screen(pixel_buffer, 1);
}

/* Display the menu GUI choices */
void menuSelector(int menuChoice)
{
	switch(menuChoice) {
		case CHOOSE_SINGLE_PLAYER:
			clearBackBuff();
			drawTronMenu();
			alt_up_pixel_buffer_dma_draw_rectangle(pixel_buffer, 17*CELL_SIZE, 14*CELL_SIZE, 24*CELL_SIZE, 15*CELL_SIZE, WHITE, 1);
			swapBuffers();
			break;
		case CHOOSE_TWO_PLAYER:
			clearBackBuff();
			drawTronMenu();
			alt_up_pixel_buffer_dma_draw_rectangle(pixel_buffer, 17*CELL_SIZE, 17*CELL_SIZE, 22*CELL_SIZE, 18*CELL_SIZE, WHITE, 1);
			swapBuffers();
			break;
		case CHOOSE_PROFILES:
			clearBackBuff();
			drawTronMenu();
			alt_up_pixel_buffer_dma_draw_rectangle(pixel_buffer, 17*CELL_SIZE, 20*CELL_SIZE, 21*CELL_SIZE, 21*CELL_SIZE, WHITE, 1);
			swapBuffers();
			break;
		case CHOOSE_TUTORIAL:
			clearBackBuff();
			drawTronMenu();
			alt_up_pixel_buffer_dma_draw_rectangle(pixel_buffer, 17*CELL_SIZE, 23*CELL_SIZE, 21*CELL_SIZE, 24*CELL_SIZE, WHITE, 1);
			swapBuffers();
			break;
		case CHOOSE_GLUTTONY:
			clearBackBuff();
			drawTronMenu();
			alt_up_pixel_buffer_dma_draw_rectangle(pixel_buffer, 17*CELL_SIZE, 26*CELL_SIZE, 21*CELL_SIZE, 27*CELL_SIZE, WHITE, 1);
			swapBuffers();
			break;
		default:
			break;
	}
}

/* Draws a pixel to the background buffer at the given xy coordinates with the given colour 
   Written by Jeffrey Goeders	*/
int draw_pixel_fast(unsigned int color, unsigned int x, unsigned int y) {
	unsigned int addr;
	addr = ((x & pixel_buffer->x_coord_mask) << 1);
	addr += (((y & pixel_buffer->y_coord_mask) * 320) << 1);

	IOWR_16DIRECT(pixel_buffer->back_buffer_start_address, addr, color);

	return 0;
}

/* Draws the etank starting at the given xy coordinate */
void drawETank(int x, int y)
{
	int index1;
	int index2;
	for(index1 = 0; index1 < CELL_SIZE; index1++)
	{
		for(index2 = 0; index2 < CELL_SIZE; index2++)
		{
			draw_pixel_fast(Etank[index2][index1], x+index1, y+index2);
		}
	}
}

/* Draws the etank starting at the given xy coordinate */
void drawSuperETank(int x, int y)
{
	int index1;
	int index2;
	for(index1 = 0; index1 < CELL_SIZE; index1++)
	{
		for(index2 = 0; index2 < CELL_SIZE; index2++)
		{
			draw_pixel_fast(superEtank[index2][index1], x+index1, y+index2);
		}
	}
}

/* Draws the Ftank starting at the given xy coordinate */
void drawFTank(int x, int y)
{
	int index1;
	int index2;
	for(index1 = 0; index1 < CELL_SIZE; index1++)
	{
		for(index2 = 0; index2 < CELL_SIZE; index2++)
		{
			draw_pixel_fast(Ftank[index2][index1], x+index1, y+index2);
		}
	}
}

/* Draw Car going North at the given xy coordinate */
void drawCarN(int x, int y, int car)
{
	int index1;
	int index2;
	for(index1 = 0; index1 < CELL_SIZE; index1++)
	{
		for(index2 = 0; index2 < CELL_SIZE; index2++)
		{
			if(car == BODY_1)
				draw_pixel_fast(tronCarCyan[index2][index1], x+index1, y+index2);
			else if(car == BODY_2)
				draw_pixel_fast(tronCarRed[index2][index1], x+index1, y+index2);
		}
	}
}

/* Draw Car going West at the given xy coordinate */
void drawCarW(int x, int y, int car)
{
	int index1;
	int index2;
	for(index1 = 0; index1 < CELL_SIZE; index1++)
	{
		for(index2 = 0; index2 < CELL_SIZE; index2++)
		{
			if(car == BODY_1)
				draw_pixel_fast(tronCarCyan[index1][index2], x+index1, y+index2);
			else if(car == BODY_2)
				draw_pixel_fast(tronCarRed[index1][index2], x+index1, y+index2);
		}
	}
}

/* Draw Car going East at the given xy coordinate */
void drawCarE(int x, int y, int car)
{
	int index1;
	int index2;
	int x2 = 0;
	for(index1 = CELL_SIZE-1; index1 >= 0; index1--)
	{
		int y2 = 0;
		for(index2 = CELL_SIZE-1; index2 >= 0; index2--)
		{
			if(car == BODY_1)
				draw_pixel_fast(tronCarCyan[index1][index2], x+x2, y+y2);
			else if(car == BODY_2)
				draw_pixel_fast(tronCarRed[index1][index2], x+x2, y+y2);
			y2++;
		}
		x2++;
	}
}

/* Draw Car going South at the given xy coordinate */
void drawCarS(int x, int y, int car)
{
	int index1;
	int index2;
	int x2 = 0;
	for(index1 = CELL_SIZE-1; index1 >= 0; index1--)
	{
		int y2 = 0;
		for(index2 = CELL_SIZE-1; index2 >= 0; index2--)
		{
			if(car == BODY_1)
				draw_pixel_fast(tronCarCyan[index2][index1], x+x2, y+y2);
			else if(car == BODY_2)
				draw_pixel_fast(tronCarRed[index2][index1], x+x2, y+y2);
			y2++;
		}
		x2++;
	}
}

int load_bmp_file(void)
{
	int loop_counter;
	menuImage = (char *) malloc(MENU_GRAPHIC_SIZE * sizeof(char));

	short int fileHandle;
	unsigned int headerInfoSize;
	fileHandle = open_file(MENU_BACKGROUND);

	//skip header in wav file - 14 bytes long
	for(loop_counter = 0; loop_counter < BMP_HEADER; loop_counter++)
	{
		if(loop_counter < BMP_HEADER)
			alt_up_sd_card_read(fileHandle);
	}

	menuImage[0] = alt_up_sd_card_read(fileHandle);
	headerInfoSize = menuImage[0];
	printf("%i\n", headerInfoSize);

	for(loop_counter = 1; loop_counter < headerInfoSize; loop_counter++)
	{
		if(loop_counter < 12)
			menuImage[loop_counter] = alt_up_sd_card_read(fileHandle);
		else
			alt_up_sd_card_read(fileHandle);
	}

	for(loop_counter = 12; loop_counter < MENU_GRAPHIC_SIZE; loop_counter++)
	{
		menuImage[loop_counter] = alt_up_sd_card_read(fileHandle);
	}

	return 1;
}

int loadMenuBackground(void)
{
	int index;
	int x,y;
	unsigned int red;
	unsigned int green;
	unsigned int blue;
	unsigned int width = 0;
	unsigned int height = 0;
	unsigned int nios_pixel_colour;

	for(index = 0; index < 4; index++)
	{
		width |= menuImage[BMP_WIDTH_OFFSET+index] << (index*8);
	}

	for(index = 0; index < 4; index++)
	{
		height |= menuImage[BMP_HEIGHT_OFFSET+index] << (index*8);
	}

	height = height & HEIGHT_MASK;
	printf("%u, %u\n", width, height);

	index = BMP_DATA_OFFSET;
	for(y = height - 1; y >= 0; y--)
	{
		for(x = 0; x < width; x++)
		{
			blue = menuImage[index];
			green = menuImage[index+1];
			red = menuImage[index+2];
			if(y == 0 && x == width-1)
				printf("UNSHIFTED:\nRed: %u, Green: %u, Blue: %u\n", red, green, blue);
			red = red >> 3;
			green = green >> 2;
			blue = blue >> 3;

			nios_pixel_colour = (((red << 6) | green) << 5) | blue;
			if(y == 0 && x == width-1)
				printf("Red: %u, Green: %u, Blue: %u, BIT SHIFTED COLOUR: %u\n", red, green, blue, nios_pixel_colour);
			tronMenu[y][x] = nios_pixel_colour;
			index += 3;
		}
	}
	return 1;
}

void drawTronMenu(void)
{
	int index1;
	int index2;
	for(index1 = 0; index1 < SCREEN_WIDTH; index1++)
	{
		for(index2 = 0; index2 < SCREEN_HEIGHT; index2++)
		{
			draw_pixel_fast(tronMenu[index2][index1], index1, index2);
//			if(index1 == 0 && index2 == 0)
//				printf("%i\n", tronMenu[index2][index1]);
		}
	}
}
