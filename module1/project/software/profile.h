#ifndef __PROFILE_H__
#define __PROFILE_H__

#include "snake.h"

#define BUFFERSIZE 5
#define IDSIZE 2
#define HIGHSCORESIZE 3

static const char FILE_NAME[] = "profile1.txt";
static const int FILE_SIZE = 38;
static const int LINE_LENGTH = 8;
static const int NUM = 5;
static const int NAMESIZE = 10;


//TODO: Incorporate key configs and other profile information later
typedef struct player {
	/* playerID should NOT be 0 */
	int playerID;
	int highscore;
} player;


/* printProfile
 * Return: Returns true on successful write to the SD card
 * Writes the player data to "profile.txt" on the SD card
 */
bool printProfile(player *p);

/* readProfile
 * Return: Returns true on successful read of a profile on the SD card
 * Reads the profile information of player "id" and stores the information in p
 * NOTE: Player ID should NOT be 0
 */
bool readProfile(player *p, int id);

/* printHighscore
 * Reads the player profiles from the SD card 1 by 1, and prints to screen
 */
void printHighscore(char* buffer1,char* buffer2,char* buffer3,char* buffer4,char* buffer5);
void printHighscoreScreen(char* buffer1,char* buffer2,char* buffer3,char* buffer4,char* buffer5);

void readHighscore(char* buffer);

void writeFile(void);

/* readProfile
 * prints to the Nios II Console
 */
void read_profiles(void);

/* checkHighscore
 * Return: Returns true on successful read of a profile on the SD card
 * Reads the profile information of player "id" and stores the information in p
 * if the score passed into the function is greater than the highscore read from the
 * file on the SD card, the function calls writeHighscore to update the SD Card file
 */
bool checkHighscore(int score, int id);

/* writeHighscore
 * Return: Returns true on successful read of a profile on the SD card
 * writes score to the SD card file for the specific playerID
 */
bool writeHighscore(int id, int score);

bool writeProfile(char* name, char* idstring);


#endif
