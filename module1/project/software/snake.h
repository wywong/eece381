#ifndef __SNAKE_H__
#define __SNAKE_H__

#include "queue.h"

/* Grid Constants */
#ifndef DEBUG

#define NROW 30
#define NCOL 41
#define WALL_CELL 100
#define FOOD_CELL 101
#define P2FOOD_CELL 102
#define SUPERFOOD_CELL 103

#else

#define NROW 10
#define NCOL 10
#define WALL_CELL 8
#define FOOD_CELL 9

#endif

#define NUM_PLAYERS 2

#define EMPTY_CELL 0
#define BODY_1 1
#define HEAD_N_1 2
#define HEAD_E_1 3
#define HEAD_S_1 4
#define HEAD_W_1 5

#define BODY_2 6
#define HEAD_N_2 7
#define HEAD_E_2 8
#define HEAD_S_2 9
#define HEAD_W_2 10

#define FPS 10

typedef enum{NORTH, EAST, SOUTH, WEST} direction;

typedef struct snake {
	int playerID;
	int profileID;
	int speedFactor;
	int score;
	queue* body;
	direction dir;
	int growCount;
} snake;

/* Creates a new snake */
snake* createSnake(int id, int speed, direction d, int x, int y, int length);

/* Update snake head */
void updateHead(snake* s);

/* Updates the snake by modifying the appropriate coordinates on the grid
 * returns true if snake is still alive after the call */
bool snakeUpdate(snake* s);

/* peekAhead
 * Return: Returns the state of the grid block directly ahead of the snake
 * Assigns the new coordinates of the snake head to newHead
 */
int peekAhead(direction d, point *head, point *newHead);

/* Kills snake */
void snakeDie(snake* s);

/* Food generator
 * Return: returns TRUE if food was successfully generated, FALSE otherwise
 * Sets the value of the block to "food" type if is currently empty
 */
int foodGen(void);

/* Food generator (2 player mode)
 * Return: returns TRUE if food was successfully generated, FALSE otherwise
 * Sets the value of the block to "P2FOOD_CELL" type if is currently empty.
 * P2FOOD_CELL only increases the length of the snake
 */
int P2foodGen(void);

/* printGrid
 * Helper function that prints the states of each block in the grid
 */
void printGrid(void);

/* resetGrid
 * Helper function that reset the grid */
void resetGrid(void);

/* Single player
 * Play single player mode */
void singlePlayer(void);

/* Two player
 * Play two player mode */
void twoPlayer(void);

/* Tutorial
 * Play tutorial mode */
void tutorial(void);

/* Tutorial stages */
void rightStage(snake *s1);
void leftStage(snake *s1);
void foodStage(snake *s1);

/* Gluttony game mode */
void gluttony(void);

#endif /* __SNAKE_H__ */

