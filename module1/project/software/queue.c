/* Filename: queue.c */

#include "queue.h"
#include <stdlib.h>

/* Creates and returns an empty queue */
queue* createQueue() {
	queue* newQ = (queue*) malloc(sizeof(struct queue));
	newQ->start = 0;
	newQ->end = 0;
	newQ->size = 0;

	return newQ;
}

/* Checks if queue is empty
 * Returns true if empty.
 * Otherwise returns false */
bool isEmptyQueue(queue* q) {
	return (q->size <= 0) ? true : false;
}

/* enqueue point (x,y) to the end of the queue
 * Return true on success, false if queue is full */
bool enqueue(queue* q, int x, int y) {
	int endInd;
	if(isEmptyQueue(q)) {
		(q->buffer)[q->start][X_COORD] = x;
		(q->buffer)[q->start][Y_COORD] = y;

		q->end = q->start;

		(q->size)++;
		return true;
	} else if(q->size < CIRC_BUF_SIZE) {
		q->end = (q->end + 1) % CIRC_BUF_SIZE;

		endInd = q->end;
		(q->buffer)[endInd][X_COORD] = x;
		(q->buffer)[endInd][Y_COORD] = y;

		(q->size)++;
		return true;
	} else {
		return false;
	}
}

/* dequeue head of the queue
 * Return true on success, false if queue was empty */
bool dequeue(queue* q) {
	if(!isEmptyQueue(q)) {
		q->start = (q->start + 1) % CIRC_BUF_SIZE;
		(q->size)--;
		return true;
	} else {
		return false;
	}
}

/* peek at the head of the queue
 * store the result in point p
 * Return false if queue is empty, true otherwise */
bool peek_head(queue* q, point* p) {
	if(isEmptyQueue(q)) {
		return false;
	} else {
		p->x = q->buffer[q->start][X_COORD];
		p->y = q->buffer[q->start][Y_COORD];
		return true;
	}
}

/* peek at the tail of the queue
 * store the result in point p
 * Return false if queue is empty, true otherwise */
bool peek_tail(queue* q, point* p) {
	if(isEmptyQueue(q)) {
		return false;
	} else {
		p->x = (q->buffer)[q->end][X_COORD];
		p->y = (q->buffer)[q->end][Y_COORD];
		return true;
	}
}
