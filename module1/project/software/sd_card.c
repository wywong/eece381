/*
 * sd_card.c
 *
 *  Created on: 2014-02-06
 *      Author: Brendan
 */

#include "sd_card.h"
#include <stdio.h>
#include <stdlib.h>

alt_up_sd_card_dev * SDcard = NULL;

//opens SD card reader
void SD_setup(){
	//open the SD card device
	SDcard = alt_up_sd_card_open_dev("/dev/Altera_UP_SD_Card_Avalon_Interface_0");

	//check that SD card was opened properly
	if (SDcard == NULL)
		alt_printf("Error: could not open SD Card =\n");
	else
		alt_printf("Opened SD Card\n");
}

//opens files from SD Card and returns a file handle
short int open_file(char* FILE_NAME) {

	short int fileHandle;

	if(SDcard != NULL) {

		if(alt_up_sd_card_is_Present()) {
			printf("Card connected.\n");

			if(alt_up_sd_card_is_FAT16()) {
				//printf("FAT16 file system detected.\n");
				printf("Loading File: %s \n", FILE_NAME);

				fileHandle = alt_up_sd_card_fopen(FILE_NAME, false);

				if ( fileHandle < 0 ){
					printf("Could not open file %s \n", FILE_NAME);
				}

				return fileHandle;
			}
		}
	}

	return -1;
}

bool close_file(short int fileHandle){
	alt_up_sd_card_fclose(fileHandle);
}





