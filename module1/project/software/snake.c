/* Filename: snake.c */

#ifndef DEBUG
#include "tron_graphics.h"
#endif

#include "menu.h"
#include "profile.h"
#include "snake.h"
#include "system.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "altera_avalon_pio_regs.h"
#include "altera_avalon_timer_regs.h"
#include "altera_avalon_timer.h"
#include "altera_nios2_qsys_irq.h"
#include "sys/alt_irq.h"

extern int nextDirS1;
extern int nextDirS2;
extern int *curDirS1;
extern int *curDirS2;
extern int menu;
extern char* playerDied;
extern int newHighScoreFlag = 0;
int setScore = 0;
int setID = 0;
int priority;

#define TIMER_0_BASE 0x00004400
#define TIMER_STOP (1 << 3)
#define TIMER_START (1 << 2)
#define TIMER_CONT (1 << 1)
#define TIMER_ITO (1 << 0)

extern int newID;

extern char buffer1[30];
extern char buffer2[30];
extern char buffer3[30];
extern char buffer4[30];
extern char buffer5[30];

#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
static void handle_timer_interrupts(void* context);
#else
static void handle_timer_interrupts(void* context, alt_u32 id);
#endif

void init_timer_interrupt(void);

/* Grid initialized to 0
 *
 * Index for grid values:
 * 		0	Empty
 * 		1-4	Reserved for snake cells
 *		100 Wall
 * 		101 Food
 */

void init_timer_interrupt(void)
{
		IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE, TIMER_STOP | TIMER_ITO);
		IOWR_ALTERA_AVALON_TIMER_STATUS(TIMER_0_BASE,0);// Clear TO Bit(Reaching 0)
		alt_irq_register(TIMER_0_IRQ, 0x0, handle_timer_interrupts); //Register Interrupt
		IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE, TIMER_START | TIMER_ITO );//Start Timer, IRQ enable
}

#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
static void handle_timer_interrupts(void* context)
{
	menu = GAME_OVER_SCREEN;
	IOWR_ALTERA_AVALON_TIMER_STATUS(TIMER_0_BASE,0);
}
#else
static void handle_timer_interrupts(void* context, alt_u32 id)
#endif
{
	menu = GAME_OVER_SCREEN;
	IOWR_ALTERA_AVALON_TIMER_STATUS(TIMER_0_BASE,0);
}

extern int GRID[NROW][NCOL];
/* Flag that shows if food has already been generated on the grid */
int FOODSTATE = false;
int powerupCount = 0;

bool STAGECOMPLETE = false;

/* Creates a new snake */
snake* createSnake(int id, int speed, direction d, int x, int y, int length) {
	snake* s = (snake*) malloc(sizeof(snake));
	s->playerID = id;
	s->profileID = newID;
	s->score = 0;
	s->speedFactor = speed;
	s->dir = d;
	s->growCount = 0;

	s->body = createQueue();

	int i;
	if( (d == NORTH) || (d == WEST) ) {
		for(i = length-1; i >= 0; --i) {
			GRID[x][y+i] = id;
			enqueue(s->body, x, y+i);
		}
	} else {
		for(i = 0; i < length; ++i) {
			GRID[x][y+i] = id;
			enqueue(s->body, x, y+i);
		}
	}

	FOODSTATE = false;

	return s;
}

/* Update snake head */
void updateHead(snake* s) {
	point snakeHead;
	point nextHead;
	peek_tail(s->body, &snakeHead);

	int id = s->playerID;

	GRID[snakeHead.x][snakeHead.y] = (id == BODY_1) ? BODY_1 : BODY_2;

	peekAhead(s->dir, &snakeHead, &nextHead);
	enqueue(s->body, nextHead.x, nextHead.y);
	int head;
	if(id == BODY_1) {
		switch(s->dir) {
			case NORTH:
				head = HEAD_N_1;
				break;
			case EAST:
				head = HEAD_E_1;
				break;
			case SOUTH:
				head = HEAD_S_1;
				break;
			default:
				head = HEAD_W_1;
				break;
		}
	} else {
		switch(s->dir) {
			case NORTH:
				head = HEAD_N_2;
				break;
			case EAST:
				head = HEAD_E_2;
				break;
			case SOUTH:
				head = HEAD_S_2;
				break;
			default:
				head = HEAD_W_2;
				break;
		}
	}
	GRID[nextHead.x][nextHead.y] = head;
}

/* Updates the snake by modifying the appropriate coordinates on the grid
 * returns true if snake is still alive after the call */
bool snakeUpdate(snake* s) {
	point snakeHead;
	point nextHead;
	point snakeTail;
	int blockState;

	peek_tail(s->body, &snakeHead);
	peek_head(s->body, &snakeTail);
	blockState = peekAhead(s->dir, &snakeHead, &nextHead);

	switch(blockState) {
		case EMPTY_CELL:
			updateHead(s);
			if(s->growCount == 0) {
				dequeue(s->body);
				GRID[snakeTail.x][snakeTail.y] = EMPTY_CELL;
			} else {
				s->growCount--;
			}
			return true;
		case FOOD_CELL:
			s->speedFactor = (s->speedFactor > 1) ? (s->speedFactor)-1 : 1;
			(s->score)++;
			updateHead(s);
			FOODSTATE = false;
			play_food();
			return true;
		case P2FOOD_CELL:
			updateHead(s);
			FOODSTATE = false;
			play_food();
			return true;
		case SUPERFOOD_CELL:
			updateHead(s);
			s->growCount = 5;
			FOODSTATE = false;
			play_food();
			return true;
		default:
			if(menu == SINGLE_PLAYER_SCREEN){
				setScore = s->score;
				setID = s->profileID;
				checkHighscore(s->score, s->profileID);
				read_profiles();
				printHighscore( buffer1, buffer2, buffer3, buffer4, buffer5);
			}
			play_death();
			snakeDie(s);

			return false;
	}
}

/* peekAhead
 * Return: Returns the state of the grid block directly ahead of the snake
 */
int peekAhead(direction d, point *head, point *newHead) {
	switch(d) {
		case NORTH:
			newHead->x = head->x - 1;
			newHead->y = head->y;
			return GRID[newHead->x][newHead->y];
		case EAST:
			newHead->x = head->x;
			newHead->y = head->y + 1;
			return GRID[newHead->x][newHead->y];
		case SOUTH:
			newHead->x = head->x + 1;
			newHead->y = head->y;
			return GRID[newHead->x][newHead->y];
		default:
			newHead->x = head->x;
			newHead->y = head->y - 1;
			return GRID[newHead->x][newHead->y];
	}
}

/* Kills snake */
void snakeDie(snake* s) {
#ifndef DEBUG
	resetGrid();
	if(s->playerID == BODY_1)
		playerDied = "Player 1 Defeated";
	else if(s->playerID == BODY_2)
		playerDied = "Player 2 Defeated";
	printf("DEAD SNAKE\n");
#else
	resetGrid();
#endif
	return;
}

/* foodGen
 * Return: Returns true if food was successfully generated, false otherwise
 * Sets the value of the block to "food" type if is currently empty and there
 * is no other food on the map
 */
int foodGen(void) {
	int x, y;
	srand(time(NULL) + rand());
	x = (rand() % (NROW-1)) + 1;
	y = (rand() % (NCOL-1)) + 1;
	if(GRID[x][y] == EMPTY_CELL && FOODSTATE == false) {
		GRID[x][y] = FOOD_CELL;
		FOODSTATE = true;
		return true;
	} else {
		return false;
	}
}

/* Food generator (2 player mode)
 * Return: returns TRUE if food was successfully generated, FALSE otherwise
 * Sets the value of the block to "P2FOOD_CELL" type if is currently empty.
 * P2FOOD_CELL only increases the length of the snake
 */
int P2foodGen(void) {
	int x, y;
	srand(time(NULL) + rand());
	x = (rand() % (NROW-1)) + 1;
	y = (rand() % (NCOL-1)) + 1;
	if(GRID[x][y] == EMPTY_CELL && FOODSTATE == false) {
		/* A powerup is generated after multiple food is generated */
		if(powerupCount >= 5) {
			GRID[x][y] = SUPERFOOD_CELL;
			powerupCount = 0;
		} else {
			GRID[x][y] = P2FOOD_CELL;
			powerupCount++;
		}
		FOODSTATE = true;
		return true;
	} else {
		return false;
	}
}

/* printGrid
 * Helper function that prints the states of each block in the grid
 */
void printGrid(void) {
	int row, col;

#ifndef DEBUG
	for(row = 1; row < NROW-1; ++row) {
		for(col = 1; col < NCOL-1; ++col) {
			switch(GRID[row][col]) {
				case BODY_1:
					drawCell((col-1)*8, (row-1)*8, CYAN);
					break;
				case BODY_2:
					drawCell((col-1)*8, (row-1)*8, RED);
					break;
				case HEAD_N_1:
					drawCarN((col-1)*8, (row-1)*8, BODY_1);
					break;
				case HEAD_E_1:
					drawCarE((col-1)*8, (row-1)*8, BODY_1);
					break;
				case HEAD_S_1:
					drawCarS((col-1)*8, (row-1)*8, BODY_1);
					break;
				case HEAD_W_1:
					drawCarW((col-1)*8, (row-1)*8, BODY_1);
					break;
				case HEAD_N_2:
					drawCarN((col-1)*8, (row-1)*8, BODY_2);
					break;
				case HEAD_E_2:
					drawCarE((col-1)*8, (row-1)*8, BODY_2);
					break;
				case HEAD_S_2:
					drawCarS((col-1)*8, (row-1)*8, BODY_2);
					break;
				case HEAD_W_2:
					drawCarW((col-1)*8, (row-1)*8, BODY_2);
					break;
				case FOOD_CELL:
					drawETank((col-1)*8, (row-1)*8);
					break;
				case P2FOOD_CELL:
					drawETank((col-1)*8, (row-1)*8);
					break;
				case SUPERFOOD_CELL:
					drawSuperETank((col-1)*8, (row-1)*8);
					break;
				default:
					drawCell((col-1)*8, (row-1)*8, BLACK);
					break;
			}
		}
	}
	drawBorder();
	swapBuffers();
#else
	for(row = 0; row < NROW; row++) {
		for(col = 0; col < NCOL; col++) {
			printf("%i", GRID[row][col]);
		}
		printf("\n");
	}
#endif
}

void resetGrid( void ) {
	int row, col;
	/* Wall border */
	for(col = 0; col < NCOL; col++) GRID[0][col] = WALL_CELL;
	for(col = 0; col < NCOL; col++) GRID[NROW-1][col] = WALL_CELL;
	for(row = 0; row < NROW; row++) GRID[row][0] = WALL_CELL;
	for(row = 0; row < NROW; row++) GRID[row][NCOL-1] = WALL_CELL;

	for(row = 1; row < NROW - 1; ++row ){
		for(col = 1; col < NCOL - 1; ++col){
			GRID[row][col] = EMPTY_CELL;
		}
	}
}

/* Single player
 * Play single player mode */
void singlePlayer(void) {
	char playerIdentity[20];

	char scoreBuffer[4];

	printf("SNAKE!!\n");
	int count1 = 0;
	snake* s1 = createSnake(BODY_1, FPS, EAST, NROW/2, NCOL/2, 4);
	curDirS1 = &(s1->dir);
	nextDirS1 = *curDirS1;
	sprintf(playerIdentity, "Player %i: ", s1->profileID);
	writeString(playerIdentity, 0, 58);
	while(menu == SINGLE_PLAYER_SCREEN) {
		foodGen();
		printGrid();
		sprintf(scoreBuffer, "%d", s1->score);
		writeString((char*) scoreBuffer, 11, 58);
		//usleep(1000);
		if(count1 == 0) {
			s1->dir = nextDirS1;
			if(!snakeUpdate(s1)) break;
		}
		count1 = (count1 + 1) % s1->speedFactor;
	}


	printGrid();
	free(s1);
}

/* Two player
 * Play two player mode */
void twoPlayer(void) {
//	writeString(PLAYER1_HUD, 0, 58);
//	writeString(PLAYER2_HUD, 14, 58);
//	char scoreBuffer[4];

	printf("SNAKE!!\n");
	int count1 = 0;
	int count2 = 0;
	snake* s1 = createSnake(BODY_1, 4, EAST, NROW/2, NCOL/2, 3);
	snake* s2 = createSnake(BODY_2, 4, WEST, NROW/2 - 1, NCOL/2, 3);
	curDirS1 = &(s1->dir);
	curDirS2 = &(s2->dir);
	nextDirS1 = *curDirS1;
	nextDirS2 = *curDirS2;
	while(menu == TWO_PLAYER_SCREEN) {
		P2foodGen();
		printGrid();
/*
		sprintf(scoreBuffer, "%d", s1->score);
		writeString((char*) scoreBuffer, 11, 58);

		sprintf(scoreBuffer, "%d", s2->score);
		writeString((char*) scoreBuffer, 25, 58);
*/
		srand(time(NULL) + rand());
		priority = (rand() % 2);
		if(priority == 0)
		{
			if(count1 == 0) {
				s1->dir = nextDirS1;
				if(!snakeUpdate(s1)) break;
			}
			if(count2 == 0) {
				s2->dir = nextDirS2;
				if(!snakeUpdate(s2)) break;
			}
		}
		else if(priority == 1)
		{
			if(count2 == 0) {
				s2->dir = nextDirS2;
				if(!snakeUpdate(s2)) break;
			}
			if(count1 == 0) {
				s1->dir = nextDirS1;
				if(!snakeUpdate(s1)) break;
			}
		}
		count1 = (count1 + 1) % s1->speedFactor;
		count2 = (count2 + 1) % s2->speedFactor;
	}
	printGrid();
	free(s1);
	free(s2);
}

/* Tutorial
 * Play tutorial mode */
void tutorial() {
	printf("SNAKE!!\n");
	int count1 = 0;
	snake* s1 = createSnake(BODY_1, FPS, NORTH, NROW/2, NCOL/2, 4);
	curDirS1 = &(s1->dir);
	nextDirS1 = *curDirS1;
	printGrid();

	writeString("Welcome to the snake tutorial!", 0, 58);
	usleep(3000000);

	STAGECOMPLETE = true;
	if(STAGECOMPLETE == true) rightStage(s1);
	if(STAGECOMPLETE == true) leftStage(s1);
	if(STAGECOMPLETE == true) foodStage(s1);
	if(STAGECOMPLETE == true) {
		writeString("Congragulations, you've completed the tutorial, enjoy the game!", 0, 58);
		usleep(3000000);
	}

	printGrid();
	free(s1);
}

/* Tutorial stages */
void rightStage(snake *s1) {
	int count1 = 0;
	clearCharBuff();
	writeString("Press the right arrow key to turn right.", 0, 58);
	STAGECOMPLETE = false;

	while(STAGECOMPLETE == false) {
		printGrid();
		if(count1 == 0) {
			if(nextDirS1 == EAST) {
				s1->dir = nextDirS1;
				STAGECOMPLETE = true;
				return;
			}
			if(!snakeUpdate(s1)) return;
		}
		count1 = (count1 + 1) % s1->speedFactor;
	}
}

void leftStage(snake *s1) {
	int count1 = 0;
	clearCharBuff();
	writeString("Press the left arrow key to turn left.", 0, 58);
	STAGECOMPLETE = false;

	while(STAGECOMPLETE == false) {
		printGrid();
		if(count1 == 0) {
			if(nextDirS1 == NORTH) {
				s1->dir = nextDirS1;
				STAGECOMPLETE = true;
				return;
			}
			if(!snakeUpdate(s1)) return;
		}
		count1 = (count1 + 1) % s1->speedFactor;
	}
}

void foodStage(snake *s1) {
	int count1 = 0;
	clearCharBuff();
	writeString("Eating food increases your score, speed, and size.", 0, 58);
	STAGECOMPLETE = false;

	while(STAGECOMPLETE == false) {
		foodGen();
		printGrid();
		if(count1 == 0) {
			s1->dir = nextDirS1;
			if(!snakeUpdate(s1)) return;
		}
		count1 = (count1 + 1) % s1->speedFactor;
		if(s1->speedFactor == FPS / 2) {
			return;
		}
	}
}

void gluttony(void) {
	writeString(PLAYER1_HUD, 0, 58);
	writeString(PLAYER2_HUD, 14, 58);
	char scoreBuffer[4];

	printf("SNAKE!!\n");
	int count1 = 0;
	int count2 = 0;
	snake* s1 = createSnake(BODY_1, 1, EAST, NROW/2, NCOL/2, 3);
	snake* s2 = createSnake(BODY_2, 1, WEST, NROW/2 - 1, NCOL/2, 3);
	curDirS1 = &(s1->dir);
	curDirS2 = &(s2->dir);
	nextDirS1 = *curDirS1;
	nextDirS2 = *curDirS2;
	init_timer_interrupt();
	while(menu == GLUTTONY_SCREEN) {
		foodGen();
		printGrid();

		sprintf(scoreBuffer, "%d", s1->score);
		writeString((char*) scoreBuffer, 11, 58);

		sprintf(scoreBuffer, "%d", s2->score);
		writeString((char*) scoreBuffer, 25, 58);

		if(count1 == 0) {
			s1->dir = nextDirS1;
			if(!snakeUpdate(s1)) break;
		}
		if(count2 == 0) {
			s2->dir = nextDirS2;
			if(!snakeUpdate(s2)) break;
		}
		count1 = (count1 + 1) % s1->speedFactor;
		count2 = (count2 + 1) % s2->speedFactor;
	}
	printGrid();
	free(s1);
	free(s2);

}
