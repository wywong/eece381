#ifndef __MENU_H__
#define __MENU_H__

#define MAIN_MENU 0
#define PROFILE_SCREEN 1
#define PROFILE_EDIT 2
#define SINGLE_PLAYER_SCREEN 3
#define TWO_PLAYER_SCREEN 4
#define TUTORIAL_SCREEN 5
#define GLUTTONY_SCREEN 6
#define GAME_OVER_SCREEN 10

#define INITIALIZING "Incoming Game"
#define TITLE "SNAKE"

#define NUM_MENU_OPTIONS 5
#define CHOOSE_SINGLE_PLAYER 0
#define CHOOSE_TWO_PLAYER 1
#define CHOOSE_PROFILES 2
#define CHOOSE_TUTORIAL 3
#define CHOOSE_GLUTTONY 4

#define SINGLE_PLAYER_OPTION "Single Player"
#define TWO_PLAYER_OPTION "Two Player"
#define PROFILE_OPTION "Profiles"
#define TUTORIAL_OPTION "Tutorial"
#define GLUTTONY_OPTION "Gluttony"

#define HIGHSCORES "HIGHSCORES"
#define PLAYER1_HUD "Player 1: "
#define PLAYER2_HUD "Player 2: "
#define GAME_OVER "Game Over"
#define RESTART "Hit Key0 or Enter to Restart"

#define EDITPROFILE "Hit Enter To Edit Profile"

#endif /* __MENU_H__ */

