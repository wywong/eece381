/* Filename: empty_test.c */

#include <assert.h>
#include <stdlib.h>

#include "../queue.h"
#include "empty_test.h"

void empty_test() {
	queue* a = createQueue();

	assert(isEmptyQueue(a) == true);

	dequeue(a);
	assert(isEmptyQueue(a) == true);

	enqueue(a, 1, 3);
	assert(isEmptyQueue(a) == false);

	enqueue(a, 5, 3);
	assert(isEmptyQueue(a) == false);

	dequeue(a);
	assert(isEmptyQueue(a) == false);
	dequeue(a);
	assert(isEmptyQueue(a) == true);

	free(a);
}


