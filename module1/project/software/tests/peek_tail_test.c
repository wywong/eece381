/* Filename: peek_tail_test.c */

#include <assert.h>
#include <stdlib.h>

#include "../queue.h"
#include "peek_tail_test.h"

/* test peek_tail */
void peek_tail_test() {
	queue* a = createQueue();
	point p;

	assert(peek_tail(a, &p) == false);

	enqueue(a, 1, 2);

	assert(peek_tail(a, &p) == true);
	assert(p.x == 1);
	assert(p.y == 2);

	enqueue(a, 4, 2);

	assert(peek_tail(a, &p) == true);
	assert(p.x == 4);
	assert(p.y == 2);

	enqueue(a, 4, 3);

	assert(peek_tail(a, &p) == true);
	assert(p.x == 4);
	assert(p.y == 3);

	enqueue(a, 4, 2);
	enqueue(a, 5, 2);
	enqueue(a, 6, 2);

	assert(peek_tail(a, &p) == true);
	assert(p.x == 5);
	assert(p.y == 2);

	dequeue(a);

	assert(peek_tail(a, &p) == true);
	assert(p.x == 5);
	assert(p.y == 2);

	dequeue(a);
	assert(peek_tail(a, &p) == true);
	assert(p.x == 5);
	assert(p.y == 2);

	dequeue(a);
	assert(peek_tail(a, &p) == true);
	assert(p.x == 5);
	assert(p.y == 2);

	dequeue(a);
	assert(peek_tail(a, &p) == true);
	assert(p.x == 5);
	assert(p.y == 2);
	dequeue(a);
	assert(peek_tail(a, &p) == false);

	free(a);
}

