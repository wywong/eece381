/* Filename: readprofile_test.c */

#include <assert.h>
#include <stdlib.h>

#include "../profile.h"
#include "readprofile_test.h"

/* readprofile_test
 * Unit test for reading profile data
 */
void readprofile_test() {
	player p;

	assert(readProfile(&p, 1) == true);
	assert(p.playerID == 1);
	assert(p.highscore == 1);

	assert(readProfile(&p, 10) == true);
	assert(p.playerID == 10);
	assert(p.highscore == 10);

	assert(readProfile(&p, 11) == true);
	assert(p.playerID == 11);
	assert(p.highscore == 111);

	assert(readProfile(&p, 0) == false);

	return;
}
