/* Filename: enqueue_test.c */

#include <assert.h>
#include <stdlib.h>

#include "../queue.h"
#include "enqueue_test.h"

/* test enqueue */
void enqueue_test() {
	queue* a = createQueue();

	assert(enqueue(a, 0, 5) == true);
	assert(a->size == 1);
	assert(a->start == 0);
	assert(a->end == 0);
	assert((a->buffer)[a->start][X_COORD] == 0);
	assert((a->buffer)[a->start][Y_COORD] == 5);
	assert((a->buffer)[a->end][X_COORD] == 0);
	assert((a->buffer)[a->end][Y_COORD] == 5);

	assert(enqueue(a, 0, 7) == true);
	assert(a->size == 2);
	assert(a->start == 0);
	assert(a->end == 1);
	assert((a->buffer)[a->start][X_COORD] == 0);
	assert((a->buffer)[a->start][Y_COORD] == 5);
	assert((a->buffer)[a->end][X_COORD] == 0);
	assert((a->buffer)[a->end][Y_COORD] == 7);

	assert(enqueue(a, 1, 5) == true);
	assert(a->size == 3);
	assert(a->start == 0);
	assert(a->end == 2);
	assert((a->buffer)[a->start][X_COORD] == 0);
	assert((a->buffer)[a->start][Y_COORD] == 5);
	assert((a->buffer)[a->end][X_COORD] == 1);
	assert((a->buffer)[a->end][Y_COORD] == 5);

	assert(enqueue(a, 1, 9) == true);
	assert(a->size == 4);
	assert(a->start == 0);
	assert(a->end == 3);

	assert(enqueue(a, 0, 7) == true);
	assert(a->size == 5);
	assert(a->start == 0);
	assert(a->end == 4);

	assert(enqueue(a, 1, 4) == false);
	assert(a->size == 5);
	assert(a->start == 0);
	assert(a->end == 4);

	free(a);
}



