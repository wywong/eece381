/* Filename: create_test.c */

#include <assert.h>
#include <stdlib.h>

#include "../queue.h"
#include "create_test.h"

/* test createQueue */
void create_test() {
	queue* a = createQueue();

	assert(a != NULL);

	assert(a->size == 0);
	assert(a->start == 0);
	assert(a->end == 0);

	free(a);
}



