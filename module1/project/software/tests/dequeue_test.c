/* Filename: dequeue_test.c */

#include <assert.h>
#include <stdlib.h>

#include "../queue.h"
#include "dequeue_test.h"

/* test dequeue */
void dequeue_test() {
	queue* a = createQueue();

	assert(dequeue(a) == false);
	enqueue(a, 1, 1);
	enqueue(a, 1, 1);
	enqueue(a, 1, 1);
	enqueue(a, 1, 1);
	enqueue(a, 2, 2);
	assert(dequeue(a) == true);
	assert(dequeue(a) == true);
	assert(dequeue(a) == true);
	assert(dequeue(a) == true);
	assert(dequeue(a) == true);

	assert(dequeue(a) == false);
	assert(dequeue(a) == false);
	free(a);
}



