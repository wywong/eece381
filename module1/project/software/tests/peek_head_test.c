/* Filename: peek_head_test.c */

#include <assert.h>
#include <stdlib.h>

#include "../queue.h"
#include "peek_head_test.h"

/* test peek_head */
void peek_head_test() {
	queue* a = createQueue();
	point p;

	assert(peek_head(a, &p) == false);

	enqueue(a, 1, 2);

	assert(peek_head(a, &p) == true);
	assert(p.x == 1);
	assert(p.y == 2);

	enqueue(a, 4, 2);

	assert(peek_head(a, &p) == true);
	assert(p.x == 1);
	assert(p.y == 2);

	enqueue(a, 4, 2);
	enqueue(a, 4, 2);
	enqueue(a, 4, 2);
	enqueue(a, 4, 2);

	assert(peek_head(a, &p) == true);
	assert(p.x == 1);
	assert(p.y == 2);

	dequeue(a);

	assert(peek_head(a, &p) == true);
	assert(p.x == 4);
	assert(p.y == 2);
	dequeue(a);
	dequeue(a);
	dequeue(a);
	dequeue(a);
	assert(peek_head(a, &p) == false);

	free(a);
}
